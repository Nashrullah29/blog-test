package main

import (
	"context"
	"flag"
	"gitlab.com/Nashrullah29/blog-test/app"
	"log"
	"os"
	"os/signal"
	"path/filepath"
	"syscall"
	"time"
)

var (
	envPath string
)

func GetCurrentDir() (string, error) {
	program := os.Args[0]
	absPath, err := filepath.Abs(program)
	if err != nil {
		return "", err
	}
	return filepath.Dir(absPath), nil
}

func InitFlag() {
	cd, err := GetCurrentDir()
	if err != nil {
		panic("can't get current directory")
	}
	flag.StringVar(&envPath, "env", filepath.Join(cd, ".env"), "path to .env file")
}

func main() {
	InitFlag()
	flag.Parse()
	srv := app.Init(envPath)
	quit := make(chan os.Signal)
	signal.Notify(quit, syscall.SIGINT, syscall.SIGTERM)
	<-quit
	log.Println("Shutdown Server ...")
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	if err := srv.Shutdown(ctx); err != nil {
		log.Fatal("Server Shutdown:", err)
	}
	select {
	case <-ctx.Done():
		log.Println("timeout of 5 seconds.")
	}
	log.Println("Server exiting")
	return
}
