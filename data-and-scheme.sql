create table public.tags
(
    id    serial
        constraint tags_pk
            primary key,
    label varchar(32) not null
        constraint tags_pk2
            unique
);

alter table public.tags
    owner to postgres;

create table public.posts
(
    id           serial
        constraint posts_pk
            primary key,
    title        varchar(256) not null,
    content      text         not null,
    status       varchar(16)  not null,
    publish_date date
);

alter table public.posts
    owner to postgres;

create table public.actors
(
    id       serial
        constraint actors_pk
            primary key,
    username varchar(32)  not null
        constraint actors_pk2
            unique,
    password varchar(256) not null,
    level    integer      not null
);

alter table public.actors
    owner to postgres;

insert into actors (username, password, level)
values ('user',  '$2a$10$bmSEeFIVN7COug7KgKTczux6A.Aejj7kq69nbsK5EQCXxYpbJtfWa', 1),
       ('admin', '$2a$10$jlchyQVQRkxrsc7IPbbJDOpddUvRoBXdAMkeMK0WufUl14msM7E.S', 0);

create table public.auth_token
(
    token    varchar(256) not null
        constraint auth_token_pk
            primary key,
    username varchar(32)  not null
        constraint auth_token_actors_username_fk
            references public.actors (username)
            on delete cascade
);

alter table public.auth_token
    owner to postgres;

create table public.post_tag
(
    tag_id  integer not null
        constraint post_tag_tags_id_fk
            references public.tags
            on delete cascade,
    post_id integer not null
        constraint post_tag_posts_id_fk
            references public.posts
            on delete cascade,
    constraint post_tag_pk
        primary key (tag_id, post_id)
);

alter table public.post_tag
    owner to postgres;

