package dto

import (
	"fmt"
	"gitlab.com/Nashrullah29/blog-test/entities"
	"gitlab.com/Nashrullah29/blog-test/utils/translate"
	"math"
	"net/http"
)

type BaseResponse struct {
	Code    int               `json:"code,omitempty"`
	Message string            `json:"message,omitempty"`
	Data    any               `json:"data,omitempty"`
	Error   map[string]string `json:"error,omitempty"`
}

func ErrorNotFound(entity string) BaseResponse {
	return BaseResponse{
		Code:    http.StatusNotFound,
		Message: fmt.Sprintf("%s not found", entity),
	}
}

func ErrorBadRequest(msgErr string) BaseResponse {
	return BaseResponse{
		Code:    http.StatusBadRequest,
		Message: msgErr,
	}
}

func ErrorValidation(err error) BaseResponse {
	return BaseResponse{
		Code:    http.StatusBadRequest,
		Message: "Invalid request parameter",
		Error:   translate.Translate(err),
	}
}

func ErrorInternalServerError() BaseResponse {
	return BaseResponse{
		Code:    http.StatusInternalServerError,
		Message: "Oops, something wrong!",
	}
}

func ErrorUnauthorizedDefault() BaseResponse {
	return ErrorUnauthorized("Unauthorized")
}

func ErrorUnauthorized(msg string) BaseResponse {
	return BaseResponse{
		Code:    http.StatusUnauthorized,
		Message: msg,
	}
}

func ErrorForbidden() BaseResponse {
	return BaseResponse{
		Code:    http.StatusForbidden,
		Message: "Forbidden",
	}
}

func Authenticated(user entities.Actor) BaseResponse {
	data := map[string]any{
		"username": user.Username,
	}
	switch {
	case user.Level == entities.User:
		data["role"] = "user"
	case user.Level == entities.Admin:
		data["role"] = "admin"
	}
	return BaseResponse{
		Code:    http.StatusOK,
		Message: "Authenticated",
		Data:    data,
	}
}

func Success(msg string, data any) BaseResponse {
	return BaseResponse{
		Code:    http.StatusOK,
		Message: msg,
		Data:    data,
	}
}

func SuccessPagination(msg string, currentPage, perPage, total int, data any) BaseResponse {
	return BaseResponse{
		Code:    http.StatusOK,
		Message: msg,
		Data: map[string]any{
			"total":      total,
			"page":       currentPage,
			"perPage":    perPage,
			"total_page": int(math.Ceil(float64(total) / float64(perPage))),
			"data":       data,
		},
	}
}

func Created(msg string, data any) BaseResponse {
	return BaseResponse{
		Code:    http.StatusCreated,
		Message: msg,
		Data:    data,
	}
}

func NeedChangePassword() BaseResponse {
	return BaseResponse{
		Code:    http.StatusForbidden,
		Message: "Please change your password!",
	}
}

func UsernameAlreadyLogin() BaseResponse {
	return BaseResponse{
		Code:    http.StatusLocked,
		Message: "Username already log in",
	}
}

func PartialError(msg string) BaseResponse {
	return BaseResponse{
		Code:    http.StatusExpectationFailed,
		Message: msg,
	}
}

func MultipleStatus(msg string, data any) BaseResponse {
	return BaseResponse{
		Code:    http.StatusMultiStatus,
		Message: msg,
		Data:    data,
	}
}
