package middleware

import (
	"github.com/gin-gonic/gin"
	"gitlab.com/Nashrullah29/blog-test/ctm"
	"gitlab.com/Nashrullah29/blog-test/dto"
	"gitlab.com/Nashrullah29/blog-test/entities"
	"net/http"
)

func AuthorizationWithRole(levels []entities.UserLevel) gin.HandlerFunc {
	return func(c *gin.Context) {
		user, err := ctm.GetUser(c)
		if err != nil {
			c.AbortWithStatusJSON(http.StatusInternalServerError, dto.ErrorInternalServerError())
			return
		}
		found := false
		for _, level := range levels {
			if level == user.Level {
				found = true
				break
			}
		}
		if !found {
			c.AbortWithStatusJSON(http.StatusForbidden, dto.ErrorForbidden())
			return
		}
	}
}

func AuthorizationAdminOnly() gin.HandlerFunc {
	return AuthorizationWithRole([]entities.UserLevel{entities.Admin})
}

func AuthorizationUserOnly() gin.HandlerFunc {
	return AuthorizationWithRole([]entities.UserLevel{entities.User})
}
