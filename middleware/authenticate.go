package middleware

import (
	"errors"
	"github.com/gin-gonic/gin"
	"gitlab.com/Nashrullah29/blog-test/ctm"
	"gitlab.com/Nashrullah29/blog-test/dto"
	"gitlab.com/Nashrullah29/blog-test/repositories"
	"net/http"
)

func Authenticate(session repositories.SessionRepository) gin.HandlerFunc {
	return func(c *gin.Context) {
		ctx := c.Copy()
		token, err := ctm.GetTokenFromContext(c)
		if err != nil {
			c.AbortWithStatusJSON(http.StatusForbidden, dto.ErrorForbidden())
			return
		}
		actor, err := session.GetUser(ctx, token)
		if err != nil {
			switch {
			case errors.Is(err, repositories.ErrDoesntExist):
				c.AbortWithStatusJSON(http.StatusForbidden, dto.ErrorForbidden())
				return
			default:
				c.AbortWithStatusJSON(http.StatusInternalServerError, dto.ErrorInternalServerError())
				return
			}
		}
		ctm.SetUser(c, actor)
	}
}
