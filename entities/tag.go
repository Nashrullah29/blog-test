package entities

type Tag struct {
	Id    int64  `json:"id"`
	Label string `json:"label"`
}
