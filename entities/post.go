package entities

import "time"

type Post struct {
	Id          int64     `json:"id"`
	Title       string    `json:"title"`
	Content     string    `json:"content"`
	Status      string    `json:"status"`
	Tags        []string  `json:"tags"`
	PublishDate time.Time `json:"publish_date"`
}
