package entities

type UserLevel uint

const (
	Admin UserLevel = iota
	User
)

type Actor struct {
	Username string
	Password string
	Level    UserLevel
}
