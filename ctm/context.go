package ctm

import (
	"context"
	"errors"
	"github.com/gin-gonic/gin"
	"gitlab.com/Nashrullah29/blog-test/entities"
	"strings"
)

var (
	ErrDoesntExist   = errors.New("key doesn't exist")
	ErrInvalidFormat = errors.New("invalid format data")
)

func GetUser(c *gin.Context) (entities.Actor, error) {
	return GetUserFromContext(c.Copy())
}

func GetUserFromContext(ctx context.Context) (entities.Actor, error) {
	userContext := ctx.Value("user")
	if userContext == nil {
		return entities.Actor{}, errors.New("can't get user from context")
	}
	user, ok := userContext.(*entities.Actor)
	if !ok {
		return entities.Actor{}, errors.New("user in ctm is not type of entities.actor")
	}
	return *user, nil
}

func SetUser(c *gin.Context, user entities.Actor) {
	c.Set("user", &user)
}

func GetTokenFromContext(c *gin.Context) (string, error) {
	auth := c.Request.Header.Get("Authorization")
	if auth == "" {
		return "", ErrDoesntExist
	}
	token := strings.TrimPrefix(auth, "Bearer ")
	if token == auth {
		return "", ErrInvalidFormat
	}
	return token, nil
}
