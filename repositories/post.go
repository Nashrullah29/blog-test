package repositories

import (
	"context"
	"database/sql"
	"fmt"
	"gitlab.com/Nashrullah29/blog-test/entities"
	"gitlab.com/Nashrullah29/blog-test/modules/post"
	"log"
	"strconv"
	"strings"
	"time"
)

var _ post.PostRepository = PostRepository{}

type PostRepository struct {
	db *sql.DB
}

func NewPostRepository(db *sql.DB) PostRepository {
	return PostRepository{db: db}
}

func MakeInPlaceholder(n, from int) string {
	questionMarks := make([]string, n)
	for i := 0; i < n; i++ {
		questionMarks[i] = "$" + strconv.Itoa(from+i)
	}
	return "(" + strings.Join(questionMarks, ",") + ")"
}

func ConvertToAny[T any](data []any, list ...T) []any {
	for _, t := range list {
		data = append(data, t)
	}
	return data
}

func (p PostRepository) insertTag(ctx context.Context, tx *sql.Tx, id int64, tags []string) error {
	placeholder := MakeInPlaceholder(len(tags), 1)
	query := fmt.Sprintf("INSERT INTO post_tag (select id as tag_id, %d as post_id from tags where label in %s)", id, placeholder)
	arg := ConvertToAny(make([]any, 0), tags...)
	_, err := tx.ExecContext(ctx, query, arg...)
	if err != nil {
		return err
	}
	return nil
}

func (p PostRepository) insert(ctx context.Context, data entities.Post) (entities.Post, error) {
	var id int64
	tx, err := p.db.Begin()
	if err != nil {
		return entities.Post{}, err
	}
	defer func() {
		if err := tx.Rollback(); err != nil {
			log.Printf("failed to rollback post insert transaction")
		}
	}()
	err = tx.QueryRowContext(ctx, "INSERT INTO posts (title, content, status, publish_date) VALUES ($1, $2 ,$3, $4) RETURNING id",
		data.Title, data.Content, data.Status, data.PublishDate).Scan(&id)
	if err != nil {
		return entities.Post{}, err
	}
	data.Id = id
	if err := p.insertTag(ctx, tx, id, data.Tags); err != nil {
		return entities.Post{}, err
	}
	if err := tx.Commit(); err != nil {
		return entities.Post{}, err
	}
	return data, nil
}

func (p PostRepository) update(ctx context.Context, data entities.Post) (entities.Post, error) {
	tx, err := p.db.Begin()
	if err != nil {
		return entities.Post{}, err
	}
	defer func() {
		if err := tx.Rollback(); err != nil {
			log.Printf("failed to rollback insertion post transaction")
		}
	}()
	_, err = tx.ExecContext(ctx, "UPDATE posts set title = $1, content = $2, status = $3, publish_date = $4 where id = $5",
		data.Title, data.Content, data.Status, data.PublishDate, data.Id)
	if err != nil {
		return entities.Post{}, err
	}
	_, err = tx.ExecContext(ctx, "DELETE FROM post_tag where post_id = $1", data.Id)
	if err != nil {
		return entities.Post{}, err
	}
	if err := p.insertTag(ctx, tx, data.Id, data.Tags); err != nil {
		return entities.Post{}, err
	}
	if err := tx.Commit(); err != nil {
		return entities.Post{}, err
	}
	return data, nil
}

func (p PostRepository) GetById(ctx context.Context, id int64) (entities.Post, error) {
	var (
		title, content, status string
		publishDate            time.Time
	)
	err := p.db.QueryRowContext(ctx, "SELECT title, content, status, publish_date FROM posts WHERE id = $1", id).
		Scan(&title, &content, &status, &publishDate)
	if err != nil {
		return entities.Post{}, err
	}
	postData := entities.Post{
		Id:          id,
		Title:       title,
		Content:     content,
		Status:      status,
		PublishDate: publishDate,
	}
	tags := make([]string, 0)
	row, err := p.db.QueryContext(ctx, "SELECT label FROM post_tag INNER JOIN tags ON post_tag.tag_id = tags.id WHERE post_tag.post_id = $1", id)
	if err != nil {
		return postData, err
	}
	defer func() {
		if err := row.Close(); err != nil {
			log.Printf("error when close row. error : %s", err.Error())
		}
	}()
	var label string
	for row.Next() {
		err := row.Scan(&label)
		if err != nil {
			return postData, err
		}
		tags = append(tags, label)
	}
	postData.Tags = tags
	return postData, nil
}

func (p PostRepository) Save(ctx context.Context, data entities.Post) (entities.Post, error) {
	if data.Id != 0 {
		return p.update(ctx, data)
	} else {
		return p.insert(ctx, data)
	}
}

func (p PostRepository) Delete(ctx context.Context, id int64) error {
	_, err := p.db.ExecContext(ctx, "DELETE FROM posts WHERE id = $1", id)
	if err != nil {
		return err
	}
	return nil
}

func (p PostRepository) GetByTag(ctx context.Context, label string) ([]entities.Post, error) {
	result := make([]entities.Post, 0)
	row, err := p.db.QueryContext(ctx, "select post_id from post_tag inner join tags on post_tag.tag_id = tags.id where tags.label = $1", label)
	if err != nil {
		return nil, err
	}
	defer func() {
		if err := row.Close(); err != nil {
			log.Printf("error when close row. error : %s", err.Error())
		}
	}()
	var id int64
	for row.Next() {
		if err := row.Scan(&id); err != nil {
			return nil, err
		}
		postData, err := p.GetById(ctx, id)
		if err != nil {
			return nil, err
		}
		result = append(result, postData)
	}
	return result, nil
}
