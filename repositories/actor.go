package repositories

import (
	"context"
	"database/sql"
	"gitlab.com/Nashrullah29/blog-test/entities"
	"gitlab.com/Nashrullah29/blog-test/modules/authenticate"
)

var _ authenticate.UserRepository = ActorRepository{}

type ActorRepository struct {
	db *sql.DB
}

func NewActorRepository(db *sql.DB) ActorRepository {
	return ActorRepository{db: db}
}

func (a ActorRepository) GetByUsername(ctx context.Context, username string) (entities.Actor, error) {
	var (
		usernameDb, password string
		level                entities.UserLevel
	)
	err := a.db.QueryRowContext(ctx, "SELECT username, password, level FROM actors WHERE username = $1", username).
		Scan(&usernameDb, &password, &level)
	if err != nil {
		return entities.Actor{}, err
	}
	return entities.Actor{
		Username: usernameDb,
		Password: password,
		Level:    level,
	}, nil
}
