package repositories

import (
	"context"
	"crypto/rand"
	"database/sql"
	"encoding/base64"
	"errors"
	"gitlab.com/Nashrullah29/blog-test/entities"
	"gitlab.com/Nashrullah29/blog-test/modules/authenticate"
)

var _ authenticate.Session = SessionRepository{}
var ErrDoesntExist = errors.New("token doesn't exist")

type SessionRepository struct {
	db *sql.DB
}

func NewSessionRepository(db *sql.DB) SessionRepository {
	return SessionRepository{db: db}
}

func (s SessionRepository) Generate(ctx context.Context, user entities.Actor) (string, error) {
	randByte := make([]byte, 64)
	_, err := rand.Read(randByte)
	if err != nil {
		return "", err
	}
	key := base64.StdEncoding.EncodeToString(randByte)
	_, err = s.db.ExecContext(ctx, "INSERT INTO auth_token (token, username) VALUES ($1, $2)", key, user.Username)
	if err != nil {
		return "", err
	}
	return key, nil
}

func (s SessionRepository) GetUser(ctx context.Context, token string) (entities.Actor, error) {
	var (
		username string
		level    entities.UserLevel
	)
	err := s.db.QueryRowContext(ctx, "SELECT actors.username, level FROM auth_token INNER JOIN actors on auth_token.username = actors.username where token = $1", token).Scan(&username, &level)
	if err != nil {
		switch {
		case errors.Is(err, sql.ErrNoRows):
			return entities.Actor{}, ErrDoesntExist
		default:
			return entities.Actor{}, err
		}
	}
	return entities.Actor{
		Username: username,
		Level:    level,
	}, nil
}

func (s SessionRepository) Delete(ctx context.Context, token string) error {
	_, err := s.db.ExecContext(ctx, "DELETE FROM auth_token WHERE token = $1", token)
	if err != nil {
		return err
	}
	return nil
}
