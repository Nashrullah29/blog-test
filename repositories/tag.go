package repositories

import (
	"context"
	"database/sql"
	"gitlab.com/Nashrullah29/blog-test/entities"
	"gitlab.com/Nashrullah29/blog-test/modules/tag"
)

var _ tag.TagRepository = TagRepository{}

type TagRepository struct {
	db *sql.DB
}

func (t TagRepository) IsLabelExist(ctx context.Context, label string) (bool, error) {
	var count int
	err := t.db.QueryRowContext(ctx, "SELECT count(1) FROM tags WHERE label = $1", label).Scan(&count)
	if err != nil {
		return false, err
	}
	return count > 0, nil
}

func NewTagRepository(db *sql.DB) TagRepository {
	return TagRepository{db: db}
}

func (t TagRepository) GetById(ctx context.Context, id int64) (entities.Tag, error) {
	var label string
	err := t.db.QueryRowContext(ctx, "SELECT label FROM tags WHERE id = $1", id).Scan(&label)
	if err != nil {
		return entities.Tag{}, err
	}
	return entities.Tag{
		Id:    id,
		Label: label,
	}, nil
}

func (t TagRepository) Save(ctx context.Context, data entities.Tag) (entities.Tag, error) {
	if data.Id == 0 {
		var id int64
		err := t.db.QueryRowContext(ctx, "INSERT INTO tags (label) VALUES ($1) RETURNING id", data.Label).Scan(&id)
		if err != nil {
			return entities.Tag{}, err
		}
		data.Id = id
		return data, err
	} else {
		_, err := t.db.ExecContext(ctx, "UPDATE tags SET label = $1 WHERE id = $2", data.Label, data.Id)
		if err != nil {
			return entities.Tag{}, err
		}
		return data, err
	}
}

func (t TagRepository) DeleteById(ctx context.Context, id int64) error {
	_, err := t.db.ExecContext(ctx, "DELETE FROM tags WHERE id = $1", id)
	if err != nil {
		return err
	}
	return nil
}
