package app

import (
	"errors"
	"fmt"
	"github.com/gin-gonic/gin"
	"github.com/joho/godotenv"
	"gitlab.com/Nashrullah29/blog-test/db"
	"gitlab.com/Nashrullah29/blog-test/utils/translate"
	"log"
	"net/http"
	"os"
)

func Init(envPath string) *http.Server {
	if err := godotenv.Load(envPath); err != nil {
		log.Printf("using environment variable. error when load .env file : %s\n", err)
	}

	if err := translate.RegisterTranslator(); err != nil {
		log.Fatalf("can't register translator. error: %s\n", err)
	}

	if os.Getenv("GIN_MODE") == "release" {
		gin.SetMode(gin.ReleaseMode)
	}
	engine := gin.Default()
	api := engine.Group("api")

	dsnDb := os.Getenv("DB_CONNECTION_STRING")

	if dsnDb == "" {
		log.Fatalln("DB_CONNECTION_STRING environment variable is mandatory")
	}

	if err := db.Connect(dsnDb); err != nil {
		log.Fatalln("Failed connect to database")
	}

	if err := Handle(api); err != nil {
		log.Fatalf("Error when handle call app.Handle. error: %s\n", err)
	}

	urlServe := fmt.Sprintf("%s:%s", os.Getenv("HOST"), os.Getenv("PORT"))
	log.Printf("Serve on %s\n", urlServe)
	srv := &http.Server{
		Addr:    urlServe,
		Handler: engine,
	}
	go func() {
		if err := srv.ListenAndServe(); err != nil && !errors.Is(http.ErrServerClosed, err) {
			log.Fatalf("Can't serve. error: %s\n", err)
		}
	}()

	return srv
}

func Handle(api *gin.RouterGroup) error {
	PostRoute(api)
	TagRoute(api)
	AuthRoute(api)
	return nil
}
