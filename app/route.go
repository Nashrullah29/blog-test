package app

import (
	"github.com/gin-gonic/gin"
	"gitlab.com/Nashrullah29/blog-test/db"
	"gitlab.com/Nashrullah29/blog-test/middleware"
	"gitlab.com/Nashrullah29/blog-test/modules/authenticate"
	"gitlab.com/Nashrullah29/blog-test/modules/post"
	"gitlab.com/Nashrullah29/blog-test/modules/tag"
	"gitlab.com/Nashrullah29/blog-test/repositories"
	"gitlab.com/Nashrullah29/blog-test/utils/crypto"
)

func PostRoute(router *gin.RouterGroup) {
	dbConn := db.GetConnection()
	sessionRepo := repositories.NewSessionRepository(dbConn)
	postEndpoint := router.Group("posts", middleware.Authenticate(sessionRepo))
	postRepo := repositories.NewPostRepository(dbConn)
	tagRepo := repositories.NewTagRepository(dbConn)
	useCase := post.NewUseCase(postRepo, tagRepo)
	requestHandler := post.NewRequestHandler(useCase)
	postEndpoint.POST("", middleware.AuthorizationUserOnly(), requestHandler.Create)
	postEndpoint.GET("", requestHandler.GetByTag)
	postEndpoint.POST(":id/publish", middleware.AuthorizationAdminOnly(), requestHandler.PublishPost)
	postEndpoint.PUT(":id", requestHandler.Update)
	postEndpoint.DELETE(":id", requestHandler.DeleteById)
	postEndpoint.GET(":id", requestHandler.GetById)
}

func TagRoute(router *gin.RouterGroup) {
	dbConn := db.GetConnection()
	sessionRepo := repositories.NewSessionRepository(dbConn)
	tagEndpoint := router.Group("tags", middleware.Authenticate(sessionRepo))
	tagRepo := repositories.NewTagRepository(dbConn)
	useCase := tag.NewUseCase(tagRepo)
	requestHandler := tag.NewRequestHandler(useCase)
	tagEndpoint.POST("", requestHandler.Create)
	tagEndpoint.GET(":id", requestHandler.GetById)
	tagEndpoint.PUT(":id", requestHandler.UpdateTag)
	tagEndpoint.DELETE(":id", requestHandler.DeleteById)
}

func AuthRoute(router *gin.RouterGroup) {
	dbConn := db.GetConnection()
	sessionRepo := repositories.NewSessionRepository(dbConn)
	hashService := crypto.NewBcryptHash()
	userRepo := repositories.NewActorRepository(dbConn)
	useCase := authenticate.NewUseCase(sessionRepo, userRepo, hashService)
	requestHandler := authenticate.NewRequestHandler(useCase)
	router.POST("login", requestHandler.Login)
	router.GET("logout", middleware.Authenticate(sessionRepo), requestHandler.LogOut)
}
