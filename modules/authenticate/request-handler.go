package authenticate

import (
	"github.com/gin-gonic/gin"
	"gitlab.com/Nashrullah29/blog-test/ctm"
	"gitlab.com/Nashrullah29/blog-test/dto"
	"net/http"
)

type RequestHandler struct {
	useCase UseCaseInterface
}

func NewRequestHandler(useCaseInterface UseCaseInterface) RequestHandler {
	return RequestHandler{useCase: useCaseInterface}
}

func (h RequestHandler) Login(c *gin.Context) {
	var (
		request LoginRequest
		ctx     = c.Copy()
	)
	if err := c.ShouldBindJSON(&request); err != nil {
		c.JSON(http.StatusBadRequest, dto.ErrorValidation(err))
		return
	}
	token, err := h.useCase.Login(ctx, request)
	if err != nil {
		c.JSON(http.StatusInternalServerError, dto.ErrorInternalServerError())
		return
	}
	if token == "" {
		c.JSON(http.StatusUnauthorized, dto.ErrorUnauthorized("username/password is invalid"))
		return
	}
	c.JSON(http.StatusOK, dto.Success("login success", "Bearer "+token))
}

func (h RequestHandler) LogOut(c *gin.Context) {
	ctx := c.Copy()
	token, _ := ctm.GetTokenFromContext(c)
	if err := h.useCase.Logout(ctx.Copy(), token); err != nil {
		c.JSON(http.StatusInternalServerError, dto.ErrorInternalServerError())
		return
	}
	c.JSON(http.StatusOK, dto.Success("log out success", nil))
}
