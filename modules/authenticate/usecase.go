package authenticate

import (
	"context"
	"errors"
	"gitlab.com/Nashrullah29/blog-test/entities"
	"log"
)

var (
	ErrSession  = errors.New("session error")
	ErrDatabase = errors.New("database error")
)

type Session interface {
	Generate(ctx context.Context, user entities.Actor) (string, error)
	GetUser(ctx context.Context, token string) (entities.Actor, error)
	Delete(ctx context.Context, token string) error
}

type UserRepository interface {
	GetByUsername(ctx context.Context, username string) (entities.Actor, error)
}

type HashService interface {
	Compare(plain, hashed string) error
}

type UseCase struct {
	session     Session
	userRepo    UserRepository
	hashService HashService
}

func NewUseCase(session Session, userRepo UserRepository, hashService HashService) UseCase {
	return UseCase{
		session:     session,
		userRepo:    userRepo,
		hashService: hashService,
	}
}

func (c UseCase) Login(ctx context.Context, req LoginRequest) (string, error) {
	actor, err := c.userRepo.GetByUsername(ctx, req.Username)
	if err != nil {
		log.Printf("Failed get user by username %s. error : %s", req.Username, err.Error())
		return "", ErrDatabase
	}
	if actor.Username == "" {
		return "", nil
	}
	if err := c.hashService.Compare(req.Password, actor.Password); err != nil {
		return "", nil
	}
	token, err := c.session.Generate(ctx, actor)
	if err != nil {
		return "", ErrSession
	}
	return token, nil
}

func (c UseCase) Logout(ctx context.Context, token string) error {
	if err := c.session.Delete(ctx, token); err != nil {
		log.Printf("error when delete session. error : %s", err.Error())
		return ErrSession
	}
	return nil
}
