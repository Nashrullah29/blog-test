package authenticate

import "context"

type UseCaseInterface interface {
	Login(ctx context.Context, req LoginRequest) (string, error)
	Logout(ctx context.Context, token string) error
}
