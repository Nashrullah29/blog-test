package tag

type TagRequest struct {
	Label string `json:"label" binding:"required"`
}
