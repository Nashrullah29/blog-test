package tag

import (
	"context"
	"database/sql"
	"errors"
	"gitlab.com/Nashrullah29/blog-test/entities"
	"log"
)

var (
	ErrDoesntExist = errors.New("tag not found")
	ErrDatabase    = errors.New("database error")
	ErrConflict    = errors.New("label already exist")
)

type TagRepository interface {
	GetById(ctx context.Context, id int64) (entities.Tag, error)
	IsLabelExist(ctx context.Context, label string) (bool, error)
	Save(ctx context.Context, tag entities.Tag) (entities.Tag, error)
	DeleteById(ctx context.Context, id int64) error
}

type UseCase struct {
	tagRepo TagRepository
}

func NewUseCase(repository TagRepository) UseCase {
	return UseCase{tagRepo: repository}
}

func (u UseCase) GetTagById(ctx context.Context, id int64) (entities.Tag, error) {
	tag, err := u.tagRepo.GetById(ctx, id)
	if err != nil {
		switch {
		case errors.Is(err, sql.ErrNoRows):
			return entities.Tag{}, ErrDoesntExist
		default:
			log.Printf("error database when retrieve tag with id %d\n. error : %s", id, err.Error())
			return entities.Tag{}, ErrDatabase
		}
	}
	return tag, nil
}

func (u UseCase) Create(ctx context.Context, req TagRequest) (entities.Tag, error) {
	tag := entities.Tag{Label: req.Label}
	exist, err := u.tagRepo.IsLabelExist(ctx, tag.Label)
	if err != nil {
		log.Printf("error when check existing label %s. error : %s", tag.Label, err.Error())
		return entities.Tag{}, ErrDatabase
	}
	if exist {
		return entities.Tag{}, ErrConflict
	}
	tag, err = u.tagRepo.Save(ctx, tag)
	if err != nil {
		log.Printf("error when create tag. error : %s\n", err.Error())
		return entities.Tag{}, ErrDatabase
	}
	return tag, nil
}

func (u UseCase) Update(ctx context.Context, id int64, req TagRequest) (entities.Tag, error) {
	exist, err := u.tagRepo.IsLabelExist(ctx, req.Label)
	if err != nil {
		log.Printf("error when check existing label %s. error : %s", req.Label, err.Error())
		return entities.Tag{}, ErrDatabase
	}
	if exist {
		return entities.Tag{}, ErrConflict
	}
	tag, err := u.GetTagById(ctx, id)
	if err != nil {
		return entities.Tag{}, err
	}
	tag.Label = req.Label
	if _, err := u.tagRepo.Save(ctx, tag); err != nil {
		log.Printf("error when update tag. error : %s\n", err.Error())
		return entities.Tag{}, ErrDatabase
	}
	return tag, nil
}

func (u UseCase) DeleteById(ctx context.Context, id int64) (entities.Tag, error) {
	tag, err := u.GetTagById(ctx, id)
	if err != nil {
		return entities.Tag{}, err
	}
	if err := u.tagRepo.DeleteById(ctx, id); err != nil {
		log.Printf("error when delete tag with id %d. error : %s\n", id, err.Error())
		return entities.Tag{}, ErrDatabase
	}
	return tag, nil
}
