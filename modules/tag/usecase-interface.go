package tag

import (
	"context"
	"gitlab.com/Nashrullah29/blog-test/entities"
)

type UseCaseInterface interface {
	GetTagById(ctx context.Context, id int64) (entities.Tag, error)
	Create(ctx context.Context, req TagRequest) (entities.Tag, error)
	Update(ctx context.Context, id int64, req TagRequest) (entities.Tag, error)
	DeleteById(ctx context.Context, id int64) (entities.Tag, error)
}
