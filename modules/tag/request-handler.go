package tag

import (
	"errors"
	"fmt"
	"github.com/gin-gonic/gin"
	"gitlab.com/Nashrullah29/blog-test/dto"
	"net/http"
	"strconv"
)

type RequestHandler struct {
	useCase UseCaseInterface
}

func NewRequestHandler(useCase UseCaseInterface) RequestHandler {
	return RequestHandler{useCase: useCase}
}

func (h RequestHandler) GetById(c *gin.Context) {
	var resp dto.BaseResponse
	defer func() {
		c.JSON(resp.Code, resp)
	}()
	ctx := c.Copy()
	idStr := c.Param("id")
	if idStr == "" {
		resp = dto.ErrorBadRequest("id is required")
		return
	}
	id, err := strconv.ParseInt(idStr, 10, 32)
	if err != nil {
		resp = dto.ErrorBadRequest("id must be number")
		return
	}
	if id <= 0 {
		resp = dto.ErrorBadRequest("id must be greater than 0")
		return
	}
	data, err := h.useCase.GetTagById(ctx, id)
	if err != nil {
		switch {
		case errors.Is(err, ErrDoesntExist):
			resp = dto.ErrorNotFound(fmt.Sprintf("tag with id %d not found", id))
			return
		default:
			resp = dto.ErrorInternalServerError()
			return
		}
	}
	resp = dto.Success("success get tag", data)
}

func (h RequestHandler) Create(c *gin.Context) {
	ctx := c.Copy()
	var req TagRequest
	if err := c.ShouldBindJSON(&req); err != nil {
		c.JSON(http.StatusBadRequest, dto.ErrorValidation(err))
		return
	}
	data, err := h.useCase.Create(ctx, req)
	if err != nil {
		switch {
		case errors.Is(err, ErrConflict):
			c.JSON(http.StatusBadRequest, dto.ErrorBadRequest("label already exist"))
			return
		default:
			c.JSON(http.StatusInternalServerError, dto.ErrorInternalServerError())
			return
		}
	}
	c.JSON(http.StatusCreated, dto.Created("tag created", data))
}

func (h RequestHandler) UpdateTag(c *gin.Context) {
	var resp dto.BaseResponse
	defer func() {
		c.JSON(resp.Code, resp)
	}()
	ctx := c.Copy()
	idStr := c.Param("id")
	if idStr == "" {
		resp = dto.ErrorBadRequest("id is required")
		return
	}
	id, err := strconv.ParseInt(idStr, 10, 32)
	if err != nil {
		resp = dto.ErrorBadRequest("id must be number")
		return
	}
	if id <= 0 {
		resp = dto.ErrorBadRequest("id must be greater than 0")
		return
	}
	var req TagRequest
	if err := c.ShouldBindJSON(&req); err != nil {
		c.JSON(http.StatusBadRequest, dto.ErrorValidation(err))
		return
	}
	data, err := h.useCase.Update(ctx, id, req)
	if err != nil {
		switch {
		case errors.Is(err, ErrDoesntExist):
			resp = dto.ErrorNotFound(fmt.Sprintf("tag with id %d not found", id))
			return
		case errors.Is(err, ErrConflict):
			resp = dto.ErrorBadRequest("label already exist")
			return
		default:
			resp = dto.ErrorInternalServerError()
			return
		}
	}
	resp = dto.Success("success update tag", data)
}

//type UseCaseInterface interface {
//	GetTagById(ctx context.Context, id int64) (Tag, error)
//	Create(ctx context.Context, req TagRequest) (Tag, error)
//	Update(ctx context.Context, id int64, req TagRequest) (Tag, error)
//	DeleteById(ctx context.Context, id int64) (Tag, error)
//}

func (h RequestHandler) DeleteById(c *gin.Context) {
	var resp dto.BaseResponse
	defer func() {
		c.JSON(resp.Code, resp)
	}()
	ctx := c.Copy()
	idStr := c.Param("id")
	if idStr == "" {
		resp = dto.ErrorBadRequest("id is required")
		return
	}
	id, err := strconv.ParseInt(idStr, 10, 32)
	if err != nil {
		resp = dto.ErrorBadRequest("id must be number")
		return
	}
	if id <= 0 {
		resp = dto.ErrorBadRequest("id must be greater than 0")
		return
	}
	data, err := h.useCase.DeleteById(ctx, id)
	if err != nil {
		switch {
		case errors.Is(err, ErrDoesntExist):
			resp = dto.ErrorNotFound(fmt.Sprintf("tag with id %d not found", id))
			return
		default:
			resp = dto.ErrorInternalServerError()
			return
		}
	}
	resp = dto.Success("success delete tag", data)
}
