package post

import (
	"context"
	"database/sql"
	"errors"
	"fmt"
	"gitlab.com/Nashrullah29/blog-test/entities"
	"log"
	"time"
)

var (
	ErrDoesntExist      = errors.New("post not found")
	ErrDatabase         = errors.New("database error")
	ErrLabelDoesntExist = errors.New("label doesn't exist")
)

type PostRepository interface {
	GetById(ctx context.Context, id int64) (entities.Post, error)
	Save(ctx context.Context, post entities.Post) (entities.Post, error)
	Delete(ctx context.Context, id int64) error
	GetByTag(ctx context.Context, tag string) ([]entities.Post, error)
}

type TagRepository interface {
	IsLabelExist(ctx context.Context, label string) (bool, error)
}

type UseCase struct {
	postRepo PostRepository
	tagRepo  TagRepository
}

func NewUseCase(repository PostRepository, tagRepository TagRepository) UseCase {
	return UseCase{postRepo: repository, tagRepo: tagRepository}
}

func (u UseCase) GetPostById(ctx context.Context, id int64) (entities.Post, error) {
	post, err := u.postRepo.GetById(ctx, id)
	if err != nil {
		switch {
		case errors.Is(err, sql.ErrNoRows):
			return entities.Post{}, ErrDoesntExist
		default:
			log.Printf("error database when retrieve post with id %d\n. error : %s", id, err.Error())
			return entities.Post{}, ErrDatabase
		}
	}
	return post, nil
}

func (u UseCase) Create(ctx context.Context, req CreateRequest) (entities.Post, error) {
	for _, label := range req.Tags {
		exist, err := u.tagRepo.IsLabelExist(ctx, label)
		if err != nil {
			log.Printf("error check label existing. error : %s\n", err.Error())
			return entities.Post{}, ErrDatabase
		}
		if !exist {
			return entities.Post{}, ErrLabelDoesntExist
		}
	}
	post := entities.Post{
		Title:   req.Title,
		Content: req.Content,
		Status:  "draft",
		Tags:    req.Tags,
	}
	post, err := u.postRepo.Save(ctx, post)
	if err != nil {
		log.Printf("error when create post. error : %s\n", err.Error())
		return entities.Post{}, ErrDatabase
	}
	return post, nil
}

func (u UseCase) PublishPost(ctx context.Context, id int64) (entities.Post, error) {
	post, err := u.GetPostById(ctx, id)
	if err != nil {
		return entities.Post{}, err
	}
	post.Status = "publish"
	post.PublishDate = time.Now()
	if _, err := u.postRepo.Save(ctx, post); err != nil {
		log.Printf("error when publish post with id %d. error : %s", id, err.Error())
	}
	return post, nil
}

func (u UseCase) Update(ctx context.Context, req UpdateRequest) (entities.Post, error) {
	for _, label := range req.Tags {
		exist, err := u.tagRepo.IsLabelExist(ctx, label)
		if err != nil {
			log.Printf("error check label existing. error : %s\n", err.Error())
			return entities.Post{}, ErrDatabase
		}
		if !exist {
			return entities.Post{}, ErrLabelDoesntExist
		}
	}
	post, err := u.GetPostById(ctx, req.Id)
	if err != nil {
		return entities.Post{}, err
	}
	newPost := entities.Post{
		Id:          post.Id,
		Title:       req.Title,
		Content:     req.Content,
		Status:      post.Status,
		Tags:        req.Tags,
		PublishDate: post.PublishDate,
	}
	if _, err := u.postRepo.Save(ctx, newPost); err != nil {
		log.Printf("error when update post. error : %s\n", err.Error())
		return entities.Post{}, ErrDatabase
	}
	return newPost, nil
}

func (u UseCase) DeleteById(ctx context.Context, id int64) (entities.Post, error) {
	post, err := u.GetPostById(ctx, id)
	if err != nil {
		return entities.Post{}, err
	}
	if err := u.postRepo.Delete(ctx, id); err != nil {
		log.Printf("error when delete post with id %d. error : %s\n", id, err.Error())
		return entities.Post{}, ErrDatabase
	}
	return post, nil
}

func (u UseCase) GetByTag(ctx context.Context, tag string) ([]entities.Post, error) {
	posts, err := u.postRepo.GetByTag(ctx, tag)
	if err != nil {
		fmt.Printf("error when retrieve post by tag %s. error : %s", tag, err.Error())
		return nil, ErrDatabase
	}
	return posts, nil
}
