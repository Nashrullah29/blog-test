package post

import (
	"context"
	"gitlab.com/Nashrullah29/blog-test/entities"
)

type UseCaseInterface interface {
	GetPostById(ctx context.Context, id int64) (entities.Post, error)
	Create(ctx context.Context, req CreateRequest) (entities.Post, error)
	PublishPost(ctx context.Context, id int64) (entities.Post, error)
	Update(ctx context.Context, req UpdateRequest) (entities.Post, error)
	DeleteById(ctx context.Context, id int64) (entities.Post, error)
	GetByTag(ctx context.Context, tag string) ([]entities.Post, error)
}
