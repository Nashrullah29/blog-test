package post

import (
	"errors"
	"fmt"
	"github.com/gin-gonic/gin"
	"gitlab.com/Nashrullah29/blog-test/dto"
	"net/http"
)

type RequestHandler struct {
	useCase UseCaseInterface
}

func NewRequestHandler(useCaseInterface UseCaseInterface) RequestHandler {
	return RequestHandler{useCase: useCaseInterface}
}

type idValidate struct {
	Id int64 `uri:"id" binding:"required,gt=0"`
}

func (h RequestHandler) GetById(c *gin.Context) {
	var id idValidate
	ctx := c.Copy()
	if err := c.ShouldBindUri(&id); err != nil {
		c.JSON(http.StatusBadRequest, dto.ErrorValidation(err))
		return
	}
	data, err := h.useCase.GetPostById(ctx, id.Id)
	if err != nil {
		switch {
		case errors.Is(err, ErrDoesntExist):
			c.JSON(http.StatusNotFound, dto.ErrorNotFound(fmt.Sprintf("post with id %d not found", id)))
			return
		default:
			c.JSON(http.StatusInternalServerError, dto.ErrorInternalServerError())
			return
		}
	}
	c.JSON(http.StatusOK, dto.Success("success get post", data))
}

func (h RequestHandler) Create(c *gin.Context) {
	var request CreateRequest
	ctx := c.Copy()
	if err := c.ShouldBindJSON(&request); err != nil {
		c.JSON(http.StatusBadRequest, dto.ErrorValidation(err))
		return
	}
	data, err := h.useCase.Create(ctx, request)
	if err != nil {
		switch {
		case errors.Is(err, ErrLabelDoesntExist):
			c.JSON(http.StatusBadRequest, dto.ErrorBadRequest("one of label doesn't exist"))
			return
		default:
			c.JSON(http.StatusInternalServerError, dto.ErrorInternalServerError())
			return
		}
	}
	c.JSON(http.StatusOK, dto.Success("post created", data))
}

func (h RequestHandler) PublishPost(c *gin.Context) {
	var id idValidate
	ctx := c.Copy()
	if err := c.ShouldBindUri(&id); err != nil {
		c.JSON(http.StatusBadRequest, dto.ErrorValidation(err))
		return
	}
	data, err := h.useCase.PublishPost(ctx, id.Id)
	if err != nil {
		switch {
		case errors.Is(err, ErrDoesntExist):
			c.JSON(http.StatusNotFound, dto.ErrorNotFound(fmt.Sprintf("post with id %d not found", id)))
			return
		default:
			c.JSON(http.StatusInternalServerError, dto.ErrorInternalServerError())
			return
		}
	}
	c.JSON(http.StatusOK, dto.Success("post published", data))
}

func (h RequestHandler) Update(c *gin.Context) {
	var (
		request UpdateRequest
		id      idValidate
	)
	ctx := c.Copy()
	if err := c.ShouldBindUri(&id); err != nil {
		c.JSON(http.StatusBadRequest, dto.ErrorValidation(err))
		return
	}
	if err := c.ShouldBindJSON(&request); err != nil {
		c.JSON(http.StatusBadRequest, dto.ErrorValidation(err))
		return
	}
	request.Id = id.Id
	data, err := h.useCase.Update(ctx, request)
	if err != nil {
		switch {
		case errors.Is(err, ErrLabelDoesntExist):
			c.JSON(http.StatusBadRequest, dto.ErrorBadRequest("one of label doesn't exist"))
			return
		case errors.Is(err, ErrDoesntExist):
			c.JSON(http.StatusNotFound, dto.ErrorNotFound(fmt.Sprintf("post with id %d", request.Id)))
			return
		default:
			c.JSON(http.StatusInternalServerError, dto.ErrorInternalServerError())
			return
		}
	}
	c.JSON(http.StatusOK, dto.Success("post updated", data))
}

func (h RequestHandler) DeleteById(c *gin.Context) {
	var id idValidate
	ctx := c.Copy()
	if err := c.ShouldBindUri(&id); err != nil {
		c.JSON(http.StatusBadRequest, dto.ErrorValidation(err))
		return
	}
	data, err := h.useCase.DeleteById(ctx, id.Id)
	if err != nil {
		switch {
		case errors.Is(err, ErrDoesntExist):
			c.JSON(http.StatusNotFound, dto.ErrorNotFound(fmt.Sprintf("post with id %d not found", id)))
			return
		default:
			c.JSON(http.StatusInternalServerError, dto.ErrorInternalServerError())
			return
		}
	}
	c.JSON(http.StatusOK, dto.Success("success delete post", data))
}

func (h RequestHandler) GetByTag(c *gin.Context) {
	ctx := c.Copy()
	tag := c.Query("tag")
	data, err := h.useCase.GetByTag(ctx, tag)
	if err != nil {
		c.JSON(http.StatusInternalServerError, dto.ErrorInternalServerError())
		return
	}
	c.JSON(http.StatusOK, dto.Success("success get tag", data))
}
