package db

import (
	"database/sql"
	_ "github.com/lib/pq"
	"time"
)

var db *sql.DB

func Connect(dsn string) error {
	var err error
	for i := 0; i < 10; i++ {
		db, err = sql.Open("postgres", dsn)
		if err == nil {
			break
		}
		time.Sleep(2 * time.Second)
	}
	if err != nil {
		return err
	}
	return nil
}

func GetConnection() *sql.DB {
	return db
}
