# Simple Blog API

I have deploy this API to cloud you can use it at https://adorable-encouragement-production.up.railway.app

## Requirement

- Golang 1.22+
- Postgresql 16+

## How to run

- Rename `.env.example` to `.env` and set `DB_CONNECTION_STRING`
- Execute `data-and-scheme.sql` to postgresql
- Run command `go run .`

## Information

- Account User Level
    - username : user
    - password : user
- Account Admin Level
    - username : admin
    - password : admin
- API Contract can be see at `Blog Test.postman_collection.json`